class AddAutoContentToServicesTable < ActiveRecord::Migration[5.0]
  def change
    #retro_cars
    #off_road_cars
    #technical_inspection
    
    s1 = Service.new
    s1.url = "retro_cars"
    s1.save

    s2 = Service.new
    s2.url = "off_road_cars"
    s2.save

    s3 = Service.new
    s3.url = "technical_inspection"
    s3.save
  end
end
