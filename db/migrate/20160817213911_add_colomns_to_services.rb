class AddColomnsToServices < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :contentimage1_file_name,:string
    add_column :services, :contentimage1_content_type, :string
    add_column :services, :contentimage1_file_size,:integer
    add_column :services, :contentimage1_updated_at,:datetime
    add_column :services, :contentblock1, :text
    add_column :services, :contentimage2_file_name,:string
    add_column :services, :contentimage2_content_type, :string
    add_column :services, :contentimage2_file_size,:integer
    add_column :services, :contentimage2_updated_at,:datetime
    add_column :services, :contentblock2, :text
    add_column :services, :article_title, :string
    add_column :services, :language_id, :integer, :default => 0
  end
end
