class AddColumUrlToAlbum < ActiveRecord::Migration[5.0]
  def change
    add_column :albums, :url, :string
    add_column :albums, :created_at, :datetime
    add_column :albums, :updated_at, :datetime
  end
end
