class CreateNewsItems < ActiveRecord::Migration[5.0]
  def change
    create_table :news_items do |t|
      t.string :title
      t.string :keywords
      t.text   :description
      t.text :text
      t.attachment :image
      t.timestamps
    end
  end
end
