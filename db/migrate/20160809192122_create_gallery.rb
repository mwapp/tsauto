class CreateGallery < ActiveRecord::Migration[5.0]
  def change
    create_table :galleries do |t|
      t.string :title
      t.string :keywords
      t.text   :description
      t.attachment :image
      t.integer :language_id, :default => 0
    end
  end
end
