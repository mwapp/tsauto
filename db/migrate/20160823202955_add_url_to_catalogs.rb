class AddUrlToCatalogs < ActiveRecord::Migration[5.0]
  def change
  	add_column :vintage_cars_catalogs, :url, :string
  	add_column :forestry_equipment_catalogs, :url, :string
  	add_column :hovercrafts_catalogs, :url, :string
  end
end
