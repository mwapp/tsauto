class AddColumLangugeToAlbum < ActiveRecord::Migration[5.0]
  def change
  	add_column :albums, :language_id, :integer, :default => 0
  end
end
