class AddUrlToVehiclesForMinesCatalog < ActiveRecord::Migration[5.0]
  def change
    add_column :vehicles_for_mines_catalogs, :url, :string
  end
end
