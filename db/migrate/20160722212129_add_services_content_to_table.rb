class AddServicesContentToTable < ActiveRecord::Migration[5.0]
  #/services/vehicles_for_mines
  #/services/forestry_equipment
  #/services/power_aggregators
  #/services/hovercrafts
  #/services/recycling
  #/services/auto_services
  
  def change
  	s1 = Service.new
    s1.url = "vehicles_for_mines"
    s1.save

    s2 = Service.new
    s2.url = "forestry_equipment"
    s2.save

    s3 = Service.new
    s3.url = "power_aggregators"
    s3.save

    s4 = Service.new
    s4.url = "hovercrafts"
    s4.save

    s5 = Service.new
    s5.url = "recycling"
    s5.save

    s6 = Service.new
    s6.url = "auto_services"
    s6.save
  end
end