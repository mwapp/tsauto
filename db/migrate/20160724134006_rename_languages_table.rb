class RenameLanguagesTable < ActiveRecord::Migration[5.0]
  def change
  	rename_table :table_languages, :languages
  end
end
