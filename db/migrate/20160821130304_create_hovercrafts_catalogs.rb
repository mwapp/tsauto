class CreateHovercraftsCatalogs < ActiveRecord::Migration[5.0]
  def change
    create_table :hovercrafts_catalogs do |t|
      t.string :title
      t.string :keywords
      t.text   :description
      t.attachment :image
      t.integer :language_id, :default => 0
      t.text :text
    end
  end
end
