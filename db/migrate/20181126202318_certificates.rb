class Certificates < ActiveRecord::Migration[5.0]
  def change
  	create_table :certificates do |t|
      t.string :title
      t.string :keywords
      t.text   :description
      t.string :text_title
      t.text :text
      t.timestamps
    end
  end
end
