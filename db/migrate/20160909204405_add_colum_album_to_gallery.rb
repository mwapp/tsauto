class AddColumAlbumToGallery < ActiveRecord::Migration[5.0]
  def change
  	add_column :galleries, :album_id, :integer, :default => 0
  end
end
