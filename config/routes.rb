Rails.application.routes.draw do
  get 'agreement/index'

  get 'errors/not_found'

  get 'errors/internal_server_error'

  get 'auto/index'

  mount Rich::Engine => '/rich', :as => 'rich'

  get '/:locale' => 'main#index', locale: /en/
  get "/:locale/services" => "services#index", locale: /en/
    get "/:locale/services/vehicles_for_mines" => "services#vehicles_for_mines", locale: /en/
    get "/:locale/services/forestry_equipment" => "services#forestry_equipment", locale: /en/
    get "/:locale/services/power_aggregators" => "services#power_aggregators", locale: /en/
    get "/:locale/services/hovercrafts" => "services#hovercrafts", locale: /en/
    get "/:locale/services/recycling" => "services#recycling", locale: /en/
    get "/:locale/services/auto_services" => "services#auto_services", locale: /en/
    get "/:locale/services/retro_cars" => "services#retro_cars", locale: /en/
    get "/:locale/services/off_road_cars" => "services#off_road_cars", locale: /en/
    get "/:locale/services/technical_inspection" => "services#technical_inspection", locale: /en/
    get "/:locale/services/mototechnics" => "services#mototechnics", locale: /en/
    get "/:locale/services/vehicles_for_mines/:url" => "services#vehicles_for_mines_item", locale: /en/
    get "/:locale/services/forestry_equipment/:url" => "services#forestry_equipment_item", locale: /en/
    get "/:locale/services/hovercrafts/:url" => "services#hovercrafts_item", locale: /en/
    get "/:locale/services/retro_cars/:url" => "services#retro_cars_item", locale: /en/
    get "/:locale/services/auto" => "auto#index", locale: /en/
  get "/:locale/about" => "about#index", locale: /en/
  get "/:locale/galleries/:album_url" => "gallery#index", locale: /en/
  get "/:locale/galleries" => "albums#index", locale: /en/
  get "/:locale/contacts" => "contacts#index", locale: /en/
  get "/:locale/news" => "news#index", locale: /en/
  get "/:locale/news/:news_url" => "news#index"
  get "/:locale/products" => "products#index", locale: /en/
  get "/:locale/auto" => "auto#index", locale: /en/

  get "/:locale/certificates" => "certificates#index", locale: /en/
  get "/:locale/sitemap" => "main#sitemap", locale: /en/

  get 'main/index'
  get "services" => "services#index"
    get "services/vehicles_for_mines" => "services#vehicles_for_mines"
    get "services/forestry_equipment" => "services#forestry_equipment"
    get "services/power_aggregators" => "services#power_aggregators"
    get "services/hovercrafts" => "services#hovercrafts"
    get "services/recycling" => "services#recycling"
    get "services/auto_services" => "services#auto_services"
    get "services/retro_cars" => "services#retro_cars"
    get "services/off_road_cars" => "services#off_road_cars"
    get "services/technical_inspection" => "services#technical_inspection"
    get "services/mototechnics" => "services#mototechnics"
    get "services/vehicles_for_mines/:url" => "services#vehicles_for_mines_item"
    get "services/forestry_equipment/:url" => "services#forestry_equipment_item"
    get "services/hovercrafts/:url" => "services#hovercrafts_item"
    get "services/retro_cars/:url" => "services#retro_cars_item"
    get "services/auto" => "auto#index"
  get "about" => "about#index"
  get "galleries/:album_url" => "gallery#index"
  get "galleries" => "albums#index"
  get "contacts" => "contacts#index"
  get "news" => "news#index"
  get "news/:news_url" => "news#news_item"
  get "products" => "products#index"
  get "certificates" => "certificates#index"
  get "sitemap" => "main#sitemap"
  

  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
  match "/agreement", :to => "agreement#index", :via => :all

  post "order" => "main#order"
  root 'main#index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Ckeditor::Engine => '/ckeditor'
end