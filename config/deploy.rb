# config valid only for Capistrano 3.1
lock '3.4.1'

set :user, 'deployer'

# require "highline"
# set :repo_password, proc { HighLine.new.ask("Repo password: ") {|q| q.echo = false}.to_s }

set :repo_password, ask('Repo password', "", {:echo => false})

set :application, 'tsauto'
set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"

set :repo_url, "git@bitbucket.org:mwapp/tsauto.git"

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true


set :linked_files, %w{config/database.yml}

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/assets config/unicorn}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rvm_type, :auto
set :rvm_ruby_version, '2.3.0'

# NGNIX
set :nginx_domains, "tsavto.com"
set :app_server_socket, "/tmp/unicorn.#{fetch :full_app_name}.sock"


# UNICORN
set :rails_env, fetch(:stage)
set :unicorn_config_path, "#{shared_path}/config/unicorn/#{fetch(:stage)}.rb"


after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  
end

