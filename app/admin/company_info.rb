ActiveAdmin.register CompanyInfo do
  menu label: "О компании", priority: 5

  permit_params :about
  
  form do |f|
    f.inputs "О компании" do
      f.input :about, as: :ckeditor, input_html: { ckeditor: { toolbar: 'Full' } }
    end
    f.actions
  end
end