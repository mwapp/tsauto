ActiveAdmin.register NewsItem do
  menu label: "Новости", priority: 2
  

  permit_params :title, :keywords, :description, :created_at, :language_id, :image, :text, language_attributes: [:language,  :id, :_destroy]
  
  index do
    selectable_column
    id_column
    column :title
    column :description
    column :created_at
    column :language_id
    actions
  end

  form do |f|
    f.inputs "Создание новости" do
      f.input :title
      f.input :keywords
      f.input :description
      f.input :created_at
      f.input :language_id, :label => 'Язык', as: :select, collection: Language.all.map{|l| ["#{l.language}", l.id]}
      f.file_field :image
      f.input :text, as: :ckeditor, input_html: { ckeditor: { toolbar: 'Full' } }
    end
    f.actions
  end
end