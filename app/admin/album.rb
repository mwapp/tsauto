ActiveAdmin.register Album do
  menu label: "Альбомы галереии", priority: 3, parent: "Галереи"
  permit_params :title, :keywords, :image, :description, :language_id, language_attributes: [:language,  :id, :_destroy]
  
  index do
    selectable_column
    id_column
    column :title
    column :language_id
    actions
  end

  form do |f|
    f.inputs "Добавить альбом" do
      f.input :title
      f.input :keywords
      f.file_field :image
      f.input :language_id, :label => 'Язык', as: :select, collection: Language.all.map{|l| ["#{l.language}", l.id]}
      f.input :description
    end
    f.actions
  end
end
