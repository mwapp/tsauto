ActiveAdmin.register Certificate do
  menu label: "Сертификаты", priority: 5

  permit_params :title, :keywords, :description, :text_title, :text
  
  index do
    selectable_column
    id_column
    column :title
    column :description
    column :text_title
    column :created_at
    actions
  end

  form do |f|
    f.inputs "Сертификаты" do
      f.input :title
      f.input :keywords
      f.input :description
      f.input :text_title
      f.input :text, as: :ckeditor, input_html: { ckeditor: { toolbar: 'Full' } }
    end
    f.actions
  end
end