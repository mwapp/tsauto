ActiveAdmin.register Service do
  menu label: "Страницы услуг", priority: 6
  permit_params :title, :keywords, :description, :text, :language_id, language_attributes: [:language,  :id, :_destroy] 
  
  index do
    selectable_column
    id_column
    column :url
    column :title
    actions
  end

  form do |f|
    f.inputs "Создание сервиса" do
      f.input :title
      f.input :keywords
      f.input :description
      f.input :url
      f.input :language_id, :label => 'Язык', as: :select, collection: Language.all.map{|l| ["#{l.language}", l.id]}
      f.input :text, as: :ckeditor, input_html: { ckeditor: { toolbar: 'Full' } }
    end
    f.actions
  end
end