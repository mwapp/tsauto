ActiveAdmin.register HovercraftsCatalog do
  menu label: "Судна на воздушной подушке", parent: "Каталоги услуг"
  permit_params :title, :keywords, :description, :image, :text, :language_id, language_attributes: [:language,  :id, :_destroy] 

  index :title => 'Судна на воздушной подушке' do
    selectable_column
    id_column
    column :url
    column :title
    column :language_id
    actions
  end

  form do |f|
    f.inputs "Каталог" do
      f.input :title
      f.input :keywords
      f.input :description
      f.input :url
      f.input :language_id, :label => 'Язык', as: :select, collection: Language.all.map{|l| ["#{l.language}", l.id]}
      f.file_field :image
      f.input :text, as: :ckeditor, input_html: { ckeditor: { toolbar: 'Full' } }
    end
    f.actions
  end
end