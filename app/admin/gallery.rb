ActiveAdmin.register Gallery do
  menu label: "Галерея", priority: 3, parent: "Галереи"

  permit_params :title, :keywords, :description, :language_id, :album_id, :image, language_attributes: [:language,  :id, :_destroy]
  
  index do
    selectable_column
    id_column
    column :title
    column :description
    column :created_at
    column :language_id
    actions
  end

  form do |f|
    f.inputs "Добавить изображение" do
      f.input :title
      f.input :keywords
      f.input :description
      f.input :language_id, :label => 'Язык', as: :select, collection: Language.all.map{|l| ["#{l.language}", l.id]}
      f.input :album_id, :label => 'Альбом', as: :select, collection: Album.all.map{|l| ["#{l.title}", l.id]}
      f.file_field :image
    end
    f.actions
  end
end