class ContactsController < ApplicationController
    def index
        @title = I18n.t("tags.contacts.title")
        @keywords = I18n.t("tags.contacts.keywords")
        @description = I18n.t("tags.contacts.description")
    end
end
