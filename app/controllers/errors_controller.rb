class ErrorsController < ApplicationController
  def not_found
    @title = I18n.t("tags.page404.title")
    @keywords = I18n.t("tags.page404.keywords")
    @description = I18n.t("tags.page404.description")
    render(:status => 404)
  end

  def internal_server_error
    @title = I18n.t("tags.page500.title")
    @keywords = I18n.t("tags.page500.keywords")
    @description = I18n.t("tags.page500.description")
    render(:status => 500)
  end
end
