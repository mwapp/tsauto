class AlbumsController < ApplicationController
    def index
        @title = I18n.t("tags.galleries.title")
        @keywords = I18n.t("tags.galleries.keywords")
        @description = I18n.t("tags.galleries.description")

        @albums = Album.where(language_id: session[:lang])
    end
end
