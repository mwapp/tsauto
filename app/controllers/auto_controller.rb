class AutoController < ApplicationController
  def index
    @title = I18n.t("tags.services.auto.title")
    @keywords = I18n.t("tags.services.auto.keywords")
    @description = I18n.t("tags.services.auto.description")
  end
end
