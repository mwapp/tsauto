class AboutController < ApplicationController
    def index
        @title = I18n.t("tags.about.title")
        @keywords = I18n.t("tags.about.keywords")
        @description = I18n.t("tags.about.description")

        @info = CompanyInfo.find_by(id: 1)
    end
end
