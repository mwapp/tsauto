class AgreementController < ApplicationController
  def index
    @title = I18n.t("tags.agreement.title")
    @keywords = I18n.t("tags.agreement.keywords")
    @description = I18n.t("tags.agreement.description")
  end
end
