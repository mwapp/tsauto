class MainController < ApplicationController
  def index
    @news = NewsItem.where(language_id: session[:lang]).order(created_at: :desc).limit(3)
    @images = Gallery.last(6)
  end

  def order
    re_captcha_key = Rails.application.secrets.re_captcha_key
    data = {"secret" => re_captcha_key,"response" => params['g-recaptcha-response']}
    require 'net/http'
    uri = URI.parse('https://www.google.com/recaptcha/api/siteverify')
    response = Net::HTTP.post_form(uri, data)

    content = JSON.parse(response.body)
    @re_captcha_result = content['success']
    if @re_captcha_result
      OrderMailer.order_email(params[:name], params[:mail], params[:phone], params[:message]).deliver
    end
  end

  def sitemap
    @title = I18n.t("tags.sitemap.title")
    @keywords = I18n.t("tags.sitemap.keywords")
    @description = I18n.t("tags.sitemap.description")
  end
end
