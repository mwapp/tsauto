class ServicesController < ApplicationController

  def index
    @title = I18n.t("tags.services.title")
    @keywords = I18n.t("tags.services.keywords")
    @description = I18n.t("tags.services.description")
  end

  def vehicles_for_mines
    @title = I18n.t("tags.services.vehicles_for_mines.title")
    @keywords = I18n.t("tags.services.vehicles_for_mines.keywords")
    @description = I18n.t("tags.services.vehicles_for_mines.description")

    @service = Service.find_by(url: "vehicles_for_mines")
    @items = VehiclesForMinesCatalog.where(language_id: session[:lang]).order('id ASC').all
  end

  def forestry_equipment
    @title = I18n.t("tags.services.forestry_equipment.title")
    @keywords = I18n.t("tags.services.forestry_equipment.keywords")
    @description = I18n.t("tags.services.forestry_equipment.description")

    @service = Service.find_by(url: "forestry_equipment")
    @items = ForestryEquipmentCatalog.where(language_id: session[:lang]).order('id ASC').all
  end

  def power_aggregators
    @title = I18n.t("tags.services.power_aggregators.title")
    @keywords = I18n.t("tags.services.power_aggregators.keywords")
    @description = I18n.t("tags.services.power_aggregators.description")

    @service = Service.find_by(url: "power_aggregators")
  end

  def hovercrafts
    @title = I18n.t("tags.services.hovercrafts.title")
    @keywords = I18n.t("tags.services.hovercrafts.keywords")
    @description = I18n.t("tags.services.hovercrafts.description")

    @service = Service.find_by(url: "hovercrafts")
    @items = HovercraftsCatalog.where(language_id: session[:lang]).order('id ASC').all
  end

  def recycling
    @title = I18n.t("tags.services.recycling.title")
    @keywords = I18n.t("tags.services.recycling.keywords")
    @description = I18n.t("tags.services.recycling.description")
    if params[:locale] == 'en'
      @service = Service.find_by(url: "recycling", language_id: 2)
    else
      @service = Service.find_by(url: "recycling", language_id: 1)
    end
  end

  def auto_services
    @service = Service.find_by(url: "auto_services")
    
    @title = @service.title
    @keywords = @service.keywords
    @description = @service.description
  end

  def retro_cars
    @title = I18n.t("tags.services.retro_cars.title")
    @keywords = I18n.t("tags.services.retro_cars.keywords")
    @description = I18n.t("tags.services.retro_cars.description")

    @service = Service.find_by(url: "retro_cars")
    @items = VintageCarsCatalog.where(language_id: session[:lang]).order('id ASC').all
  end

  def off_road_cars
    @title = I18n.t("tags.services.off_road_cars.title")
    @keywords = I18n.t("tags.services.off_road_cars.keywords")
    @description = I18n.t("tags.services.off_road_cars.description")

    @service = Service.find_by(url: "off_road_cars")
  end

  def technical_inspection
    @title = I18n.t("tags.services.technical_inspection.title")
    @keywords = I18n.t("tags.services.technical_inspection.keywords")
    @description = I18n.t("tags.services.technical_inspection.description")

    @service = Service.find_by(url: "technical_inspection")
  end

  def mototechnics
    @title = I18n.t("tags.services.mototechnics.title")
    @keywords = I18n.t("tags.services.mototechnics.keywords")
    @description = I18n.t("tags.services.mototechnics.description")
    if params[:locale] == 'en'
      @service = Service.find_by(url: "mototechnics", language_id: 2)
    else
      @service = Service.find_by(url: "mototechnics", language_id: 1)
    end
  end


  def vehicles_for_mines_item
    @items = VehiclesForMinesCatalog.where(language_id: session[:lang]).order('id ASC').find_by(url: params[:url])
    @title = @items.title
    @keywords = @items.keywords
    @description = @items.description
  end

  def forestry_equipment_item
    @items = ForestryEquipmentCatalog.where(language_id: session[:lang]).order('id ASC').find_by(url: params[:url])
    @title = @items.title
    @keywords = @items.keywords
    @description = @items.description
  end

  def hovercrafts_item
    @items = HovercraftsCatalog.where(language_id: session[:lang]).order('id ASC').find_by(url: params[:url])
    @title = @items.title
    @keywords = @items.keywords
    @description = @items.description
  end

  def retro_cars_item
    @items = VintageCarsCatalog.where(language_id: session[:lang]).order('id ASC').find_by(url: params[:url])
    @title = @items.title
    @keywords = @items.keywords
    @description = @items.description
  end
end
