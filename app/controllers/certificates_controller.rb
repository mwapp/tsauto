class CertificatesController < ApplicationController
    def index
        @title = I18n.t("tags.certificates.title")
        @keywords = I18n.t("tags.certificates.keywords")
        @description = I18n.t("tags.certificates.description")
        @certificate = Certificate.last
    end
end
