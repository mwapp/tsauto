class NewsController < ApplicationController
  def index
    @title = I18n.t("tags.news.title")
    @keywords = I18n.t("tags.news.keywords")
    @description = I18n.t("tags.news.description")

    @news = NewsItem.where(language_id: session[:lang]).order(created_at: :desc)
  end

  def news_item
    news_url = params[:news_url]
    @news = NewsItem.find_by(url: params[:news_url])

    @title =  @news.title
    @keywords = @news.keywords
    @description = @news.description
  end
end
