class GalleryController < ApplicationController
    def index
        album = Album.find_by(url: params[:album_url])
        @header = album.title
        @info = album.description

        @title = album.title
        @keywords = album.keywords
        @description = album.description

        @images = Gallery.where(language_id: session[:lang]).where(album_id: album.id)
    end
end
