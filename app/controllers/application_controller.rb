class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    session[:locale] = I18n.locale
    session[:lang] = session[:locale] == :en ? 2 : 1

    @title = I18n.t("tags.title")
    @keywords = I18n.t("tags.keywords")
    @description = I18n.t("tags.description")
    
    @header_alt = I18n.t("tags.images_alts.main_header")
  end

  def default_url_options(options={})
    { :locale => I18n.locale }
  end

  def rescue404
    #your custom method for errors, you can render anything you want there
    return "abs"
  end
end
