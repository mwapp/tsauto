class OrderMailer < ApplicationMailer
    default from: 'tsavtonn@yandex.ru'

    def order_email(name, email, phone, text)
        @name = name
        @email = email
        @phone = phone
        @text = text

        to_email1  = 'detroit52@mail.ru'
        to_email2  = 'mail@tsauto.nnov.ru'
        to_email3  = 'frimen3000@mail.ru'
        to_email4  = 'juliaseva2002@mail.ru'
        to_email5  = 'jager2203@mail.ru'
        mail(to: [to_email1, to_email2, to_email3, to_email4, to_email5], subject: 'tsavto - Новая заявка от клиента')
    end

end
