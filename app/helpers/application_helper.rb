module ApplicationHelper
	def locale_path_helper(url)
		if params[:locale] == 'en'
			url = '/en' + url
		end
		return url
	end

	def set_en_locale_path_helper
        url = request.path
        url = url.gsub('/en/', '/')
        return '/en' + url
	end

	def set_ru_locale_path_helper
        url = request.path
        url = url.gsub('/en/', '/')
        return url
	end
end
