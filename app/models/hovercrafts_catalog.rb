class HovercraftsCatalog < ApplicationRecord
  belongs_to :language
  
  has_attached_file :image
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png"]

  before_validation :gen_news_url

  private

  def gen_news_url
    self.url = String.to_slug_param(self.title)
  end
end
