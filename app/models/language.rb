class Language < ApplicationRecord
    has_many :news_items
    accepts_nested_attributes_for :news_items

    has_many :galleries
    accepts_nested_attributes_for :galleries

    has_many :services
    accepts_nested_attributes_for :services

    has_many :vehicles_for_mines_catalogs
    accepts_nested_attributes_for :vehicles_for_mines_catalogs

    has_many :forestry_equipment_catalog
    accepts_nested_attributes_for :forestry_equipment_catalog

    has_many :albums
    accepts_nested_attributes_for :albums
end