class Gallery < ApplicationRecord
	has_attached_file :image
	belongs_to :language
	belongs_to :album
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png"]
end