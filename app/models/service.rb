class Service < ApplicationRecord
  belongs_to :language
  
  has_attached_file :contentimage1
  validates_attachment_content_type :contentimage1, :content_type => ["image/jpg", "image/jpeg", "image/png"]

  has_attached_file :contentimage2
  validates_attachment_content_type :contentimage2, :content_type => ["image/jpg", "image/jpeg", "image/png"]
end
